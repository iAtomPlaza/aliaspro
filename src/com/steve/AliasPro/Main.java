package com.steve.AliasPro;

import cn.nukkit.command.CommandMap;
import cn.nukkit.plugin.PluginBase;
import com.steve.AliasPro.commands.AliasProCommand;
import com.steve.AliasPro.events.JoinEvent;
import com.steve.AliasPro.handler.DatabaseHandler;

public class Main extends PluginBase {

    public DatabaseHandler database;

    @Override
    public void onLoad() {
        CommandMap commandMap = this.getServer().getCommandMap();
        commandMap.register("AliasPro", new AliasProCommand("alias", this));
    }

    @Override
    public void onEnable() {
        final boolean mkdirs = this.getDataFolder().mkdirs();
        this.saveDefaultConfig();
        if (this.getConfig().getString("database-type").equals("online")) {
            this.database = new DatabaseHandler(this);
            this.database.connect();
        }
        this.getServer().getPluginManager().registerEvents(new JoinEvent(this), this);
    }

    @Override
    public void onDisable() {
        if (this.getConfig().getString("database-type").equals("online")) this.database.close();
    }
}
