package com.steve.AliasPro.events;

import cn.nukkit.Player;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.EventPriority;
import cn.nukkit.event.Listener;
import cn.nukkit.event.player.PlayerJoinEvent;
import cn.nukkit.utils.Config;
import cn.nukkit.utils.TextFormat;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.steve.AliasPro.Main;
import org.bson.Document;

import java.io.File;

public class JoinEvent implements Listener {

    private final Main plugin;

    public JoinEvent(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = false)
    public void onJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        plugin.getLogger().debug(uuid);

        if (plugin.getConfig().getString("database-type").equals("online")) {
            MongoDatabase database = plugin.database.getDatabase();
            String collectionString = plugin.getConfig().getString("collection");
            MongoCollection<Document> collection = database.getCollection(collectionString);
            Document query = collection.find(Filters.eq("uuid", uuid)).first();
            if (query == null) {
                Document document = new Document();
                document.append("uuid", uuid);
                document.append("alias", player.getName());
                collection.insertOne(document);
            } else {
                String name = query.getString("alias");
                player.setDisplayName(query.getString("alias"));
                player.setNameTag(query.getString("alias"));
                event.setJoinMessage(TextFormat.YELLOW + name + " joined the game");
            }
        } else {
            File file = new File(plugin.getDataFolder(), uuid+".yml");
            if (file.exists()){
                Config dataFile = new Config(new File(plugin.getDataFolder(), uuid+".yml"), Config.YAML);
                String name = dataFile.getString("name");
                player.setDisplayName(name);
                player.setNameTag(name);
                event.setJoinMessage(TextFormat.YELLOW + name + " joined the game");
            } else {
                Config dataFile = new Config(new File(plugin.getDataFolder(), uuid+".yml"), Config.YAML);
                dataFile.set("uuid", uuid);
                dataFile.set("name", player.getName());
                dataFile.save();
            }
        }
    }
}
