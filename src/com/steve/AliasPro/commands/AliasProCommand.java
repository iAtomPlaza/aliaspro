package com.steve.AliasPro.commands;

import cn.nukkit.Player;
import cn.nukkit.command.CommandSender;
import cn.nukkit.command.PluginCommand;
import cn.nukkit.utils.Config;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;
import com.steve.AliasPro.Main;
import org.bson.Document;

import java.io.File;

public class AliasProCommand extends PluginCommand {

    private Main plugin;

    public AliasProCommand(String name, Main plugin){
        super(name, plugin);
        String[] aliases = {"nick"};
        this.setDescription("sets your server alias!");
        this.setPermission("alias.set");
        this.setAliases(aliases);
        this.plugin = plugin;
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            if(sender.hasPermission("alias.set") || sender.isOp()) {
                if (args.length == 1) {
                    Player player = (Player) sender;
                    String uuid = player.getUniqueId().toString();

                    if (getPlugin().getConfig().getString("database-type").equals("online")) {
                        MongoDatabase database = plugin.database.getDatabase();
                        String collectionString = getPlugin().getConfig().getString("collection");
                        MongoCollection<Document> collection = database.getCollection(collectionString);
                        UpdateResult query = collection.updateOne(Filters.eq("uuid", uuid), new Document("$set", new Document("alias", args[0])));
                    } else {
                        Config dataFile = new Config(new File(getPlugin().getDataFolder(), uuid+".yml"), Config.YAML);
                        dataFile.set("name", args[0]);
                        dataFile.save();
                    }
                    player.setDisplayName(args[0]);
                    player.setNameTag(args[0]);
                    player.sendMessage("Set Alias/nickname to: "+args[0]);
                } else {
                    sender.sendMessage("Usage: { /nick : /alias } <new name>");
                }
            } else {
                sender.sendMessage("You do not have permission to set aliases!");
            }
        } else {
            getPlugin().getLogger().warning("You can only set an alias from in-game!");
        }
        return true;
    }
}
