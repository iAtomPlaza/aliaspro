package com.steve.AliasPro.handler;


import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.steve.AliasPro.Main;


public class DatabaseHandler {

    private Main plugin;
    private MongoClient mongoClient = null;
    private MongoDatabase database = null;


    public DatabaseHandler(Main plugin) {
        this.plugin = plugin;
    }

    public void connect() {
        String database = plugin.getConfig().getString("database");
        MongoClientURI uri = new MongoClientURI(plugin.getConfig().getString("uri"));
        this.mongoClient = new MongoClient(uri);
        this.database = this.mongoClient.getDatabase(database);
    }

    public void close() {
        if (this.mongoClient != null) this.mongoClient.close();
    }

    public MongoDatabase getDatabase() {
        return this.database;
    }

}
